package main

import "time"

type SMatrixApiResponse struct {
	Result string        `json:"result"`
	Output SMatrixOutput `json:"output"`
}

type SMatrixOutput struct {
	Vars []SMatrixWaspVar `json:"vars"`
}

type SMatrixWaspVar struct {
	WaspVarName  string `json:"waspVarName"`
	WaspVarValue string `json:"waspVarValue"`
}

type SMatrixData struct {
	// network
	NetworkDevice string `json:"networkDevice"`
	NetworkIP     string `json:"networkIp"`

	// time
	Time time.Time `json:"time"`

	// operation
	AutoBalance bool    `json:"autoBalance"`
	EcoOffset   float32 `json:"ecoOffset"`

	// firmware
	EnableSoftwareUpdates bool   `json:"enableSoftwareUpdates"`
	FirmwareVersion       string `json:"firmwareVersion"`

	Controllers []SMatrixController `json:"controllers"`
	Rooms       []SMatrixRoom       `json:"rooms"`
}

type SMatrixController struct {
	// Config + Info
	Name                      string `json:"name"`
	Index                     int    `json:"controllerIndex"`
	SoftwareVersion           string `json:"softwareVersion"`
	PendingSoftwareVersion    string `json:"pendingSoftwareVersion"`
	BootloaderSoftwareVersion string `json:"bootloaderSoftwareVersion"`
	HardwareType              string `json:"hardwareType"`
	RelaysConfig              int32  `json:"relaysConfig"`

	// State
	Present          bool    `json:"present"`
	TemperateAverage float32 `json:"temperatureAverage"`

	// Errors
	GeneralSystemAlarmError bool `json:"generalSystemAlarmError"`
}

type SMatrixRoom struct {
	// Config + Info
	Name            string `json:"name"`
	ControllerIndex int    `json:"controllerIndex"`
	ThermostatIndex int    `json:"thermostatIndex"`
	HardwareType    int32  `json:"hardwareType"`
	ThermostatType  int32  `json:"thermostatType"`

	// State
	ChannelPosition            int32   `json:"channelPosition"`
	SoftwareVersion            string  `json:"softwareVersion"`
	WifiInstalled              int32   `json:"wifiInstalled"`
	Present                    bool    `json:"present"`
	TemperatureCurrent         float32 `json:"temperatureCurrent"`
	TemperatureDesired         float32 `json:"temperatureDesired"`
	TemperatureDesiredMin      float32 `json:"temperatureDesiredMin"`
	TemperatureDesiredMax      float32 `json:"temperatureDesiredMax"`
	TemperatureFloorDesiredMin float32 `json:"temperatureFloorDesiredMin"`
	TemperatureFloorDesiredMax float32 `json:"temperatureFloorDesiredMax"`
	RelativeHumidity           int32   `json:"relativeHumidity"`
	RelativeHumidityDesired    int32   `json:"relativeHumidityDesired"`
	UtilizationFactor          float32 `json:"utilizationFactor"`
	EcoOffset                  float32 `json:"ecoOffset"`
	ActuatorActive             bool    `json:"actuatorActive"`

	// Errors
	BatteryError                bool `json:"batteryError"`
	ValvePositionError          bool `json:"valvePositionError"`
	AirSensorError              bool `json:"airSensorError"`
	ExternalSensorError         bool `json:"externalSensorError"`
	RelativeHumiditySensorError bool `json:"relativeHumiditySensorError"`
	RfError                     bool `json:"rfError"`
	TamperAlarm                 bool `json:"tamperAlarm"`

	// Technical
	SupplyTemperature float32 `json:"supplyTemperature"`
	PWMOutput         int32   `json:"pwmOutput"`
}
