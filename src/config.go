package main

import "time"

type EnvConfig struct {
	// Pipeline
	Version     string `env:"VERSION" default:"localDev"`
	CiCommitSha string `env:"CI_COMMIT_SHA" default:"localDev"`
	CiBuildTime string `env:"CI_BUILD_TIME" default:"localDev"`

	LogLevel string `env:"LOG_LEVEL" default:"trace"`
	Debug    bool   `env:"DEBUG" value:"false"`

	TimeZone string `env:"TIME_ZONE" default:"Europe/Berlin"`

	UponorServerIP     string        `env:"UPONOR_SERVER_IP" default:"192.168.230.169"`
	UponorPollInterval time.Duration `env:"UPONOR_POLL_INTERVAL" default:"30s"`

	InfluxEnable         bool          `env:"INFLUX_ENABLE" default:"true"`
	InfluxPublishAddress string        `env:"INFLUX_PUBLISH_ADDRESS" default:"https://influx.unverricht.net"`
	InfluxDatabase       string        `env:"INFLUX_DATABASE" default:"home"`
	InfluxUser           string        `env:"INFLUX_USER" default:"writeUser"`
	InfluxPassword       string        `env:"INFLUX_PASSWORD" default:"pw" mask:"true"`
	InfluxWriteTimeout   time.Duration `env:"INFLUX_WRITE_TIMEOUT" default:"5s"`
}
