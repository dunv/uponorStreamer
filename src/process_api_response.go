package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/dunv/ulog"
)

func processApiResponse(apiResponse *SMatrixApiResponse, printUnused bool, location *time.Location) (*SMatrixData, error) {
	// Collect
	roomDict := map[string]string{}
	controllerDict := map[string]string{}

	// Parse variables
	varDict := map[string]string{}
	remainingVars := map[string]string{}
	for _, keyValue := range apiResponse.Output.Vars {
		switch {
		case strings.HasPrefix(keyValue.WaspVarName, "cust_Controller") && strings.HasSuffix(keyValue.WaspVarName, "_Name"):
			controllerDict[strings.TrimSuffix(strings.TrimPrefix(keyValue.WaspVarName, "cust_Controller"), "_Name")] = keyValue.WaspVarValue
		case strings.HasPrefix(keyValue.WaspVarName, "cust_") && strings.HasSuffix(keyValue.WaspVarName, "_name"):
			roomDict[strings.TrimSuffix(strings.TrimPrefix(keyValue.WaspVarName, "cust_"), "_name")] = keyValue.WaspVarValue
		case strings.HasPrefix(keyValue.WaspVarName, "controller") && strings.HasSuffix(keyValue.WaspVarName, "_id"):
			continue
		case strings.HasPrefix(keyValue.WaspVarName, "C") && strings.HasSuffix(keyValue.WaspVarName, "_id"):
			continue
		default:
			varDict[keyValue.WaspVarName] = keyValue.WaspVarValue
			remainingVars[keyValue.WaspVarName] = keyValue.WaspVarValue
		}
	}

	systemTime := time.Date(
		int(tryGetInt(varDict, "sys_year", remainingVars))+2000,
		time.Month(tryGetInt(varDict, "sys_Month", remainingVars)),
		int(tryGetInt(varDict, "sys_days", remainingVars)),
		int(tryGetInt(varDict, "sys_hour", remainingVars)),
		int(tryGetInt(varDict, "sys_minutes", remainingVars)),
		int(tryGetInt(varDict, "sys_seconds", remainingVars)),
		0,
		location,
	)

	data := SMatrixData{
		NetworkDevice:         tryGetString(varDict, "cust_wifi_device", remainingVars),
		NetworkIP:             tryGetString(varDict, "cust_ip_device", remainingVars),
		Time:                  systemTime,
		FirmwareVersion:       tryGetString(varDict, "cust_SW_version_update", remainingVars),
		EnableSoftwareUpdates: tryGetBool(varDict, "cust_Enable_SW_Update", remainingVars),
		AutoBalance:           tryGetBool(varDict, "sys_autobalance", remainingVars),
		EcoOffset:             tryGetOffset(varDict, "sys_eco_mode_offset", remainingVars),
		Rooms:                 []SMatrixRoom{},
		Controllers:           []SMatrixController{},
	}

	// Remove variables
	delete(remainingVars, "sys_day")

	// Assemble
	for controllerIndex, controllerName := range controllerDict {
		index, err := strconv.Atoi(controllerIndex)
		if err != nil {
			ulog.Warnf("could not parse controllerIndex (%s)", err)
			continue
		}
		data.Controllers = append(data.Controllers, SMatrixController{
			// Config + Info
			Name:                      controllerName,
			Index:                     index,
			SoftwareVersion:           tryGetString(varDict, fmt.Sprintf("C%d_sw_version", index), remainingVars),
			PendingSoftwareVersion:    tryGetString(varDict, fmt.Sprintf("C%d_pending_sw_version", index), remainingVars),
			BootloaderSoftwareVersion: tryGetString(varDict, fmt.Sprintf("C%d_bootloader_sw_version", index), remainingVars),
			HardwareType:              tryGetString(varDict, fmt.Sprintf("C%d_hardware_type", index), remainingVars),
			RelaysConfig:              tryGetInt(varDict, fmt.Sprintf("C%d_controller_relays_config", index), remainingVars),

			// State
			Present:          tryGetBool(varDict, fmt.Sprintf("sys_controller_%d_presence", index), remainingVars),
			TemperateAverage: tryGetTemperature(varDict, fmt.Sprintf("C%d_average_room_temperature", index), remainingVars),

			// Errors
			GeneralSystemAlarmError: tryGetBool(varDict, fmt.Sprintf("C%d_stat_general_system_alarm", index), remainingVars),
		})

		// Debug
		// fmt.Println("TEST:", tryGetString(varDict, fmt.Sprintf("C%d_supply_temperature", index), remainingVars))
		// Remove variables
		delete(remainingVars, fmt.Sprintf("C%d_memory_map", index))             // always 1
		delete(remainingVars, fmt.Sprintf("C%d_outdoor_temperature", index))    // always number.Max
		delete(remainingVars, fmt.Sprintf("C%d_worst_room_temperature", index)) // always number.Max
		delete(remainingVars, fmt.Sprintf("C%d_worst_setpoint", index))         // always number.Max
		delete(remainingVars, fmt.Sprintf("C%d_average_setpoint", index))       // always number.Max
		delete(remainingVars, fmt.Sprintf("C%d_supply_temperature", index))     // always number.Max
		delete(remainingVars, fmt.Sprintf("C%d_general_purpose_input", index))  // always 3
		delete(remainingVars, fmt.Sprintf("C%d_channel_1_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_2_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_3_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_4_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_5_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_6_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_7_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_8_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_9_ave_temp", index))     // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_10_ave_temp", index))    // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_11_ave_temp", index))    // always 1
		delete(remainingVars, fmt.Sprintf("C%d_channel_12_ave_temp", index))    // always 1
	}

	// Assemble
	for roomIndex, roomName := range roomDict {
		controllerIndex, err := strconv.Atoi(string([]rune(roomIndex)[1:2]))
		if err != nil {
			ulog.Warnf("could not parse controllerIndex (%s)", err)
			continue
		}

		thermostatIndex, err := strconv.Atoi(string([]rune(roomIndex)[4:5]))
		if err != nil {
			ulog.Warnf("could not parse thermostatIndex (%s)", err)
			continue
		}

		data.Rooms = append(data.Rooms, SMatrixRoom{
			// Config + Info
			Name:            roomName,
			ControllerIndex: controllerIndex,
			ThermostatIndex: thermostatIndex,
			ChannelPosition: tryGetInt(varDict, fmt.Sprintf("C%d_T%d_channel_position", controllerIndex, thermostatIndex), remainingVars),
			HardwareType:    tryGetInt(varDict, fmt.Sprintf("C%d_T%d_hw_type", controllerIndex, thermostatIndex), remainingVars),
			ThermostatType:  tryGetInt(varDict, fmt.Sprintf("C%d_T%d_thermostat_type", controllerIndex, thermostatIndex), remainingVars),
			SoftwareVersion: tryGetString(varDict, fmt.Sprintf("C%d_T%d_sw_version", controllerIndex, thermostatIndex), remainingVars),
			WifiInstalled:   tryGetInt(varDict, fmt.Sprintf("C%d_T%d_stat_cb_wifi_installed", controllerIndex, thermostatIndex), remainingVars),

			// State
			Present:                    tryGetBool(varDict, fmt.Sprintf("C%d_thermostat_%d_presence", controllerIndex, thermostatIndex), remainingVars),
			TemperatureCurrent:         tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_room_temperature", controllerIndex, thermostatIndex), remainingVars),
			TemperatureDesired:         tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_setpoint", controllerIndex, thermostatIndex), remainingVars),
			TemperatureDesiredMin:      tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_minimum_setpoint", controllerIndex, thermostatIndex), remainingVars),
			TemperatureDesiredMax:      tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_maximum_setpoint", controllerIndex, thermostatIndex), remainingVars),
			TemperatureFloorDesiredMin: tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_minimum_floor_setpoint", controllerIndex, thermostatIndex), remainingVars),
			TemperatureFloorDesiredMax: tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_maximum_floor_setpoint", controllerIndex, thermostatIndex), remainingVars),
			RelativeHumidity:           tryGetInt(varDict, fmt.Sprintf("C%d_T%d_rh", controllerIndex, thermostatIndex), remainingVars),
			RelativeHumidityDesired:    tryGetInt(varDict, fmt.Sprintf("C%d_T%d_rh_setpoint", controllerIndex, thermostatIndex), remainingVars),
			UtilizationFactor:          tryGetFloat(varDict, fmt.Sprintf("cust_C%d_T%d_utilization_factor", controllerIndex, thermostatIndex), remainingVars),
			EcoOffset:                  tryGetOffset(varDict, fmt.Sprintf("C%d_T%d_eco_offset", controllerIndex, thermostatIndex), remainingVars),
			ActuatorActive:             tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_cb_actuator", controllerIndex, thermostatIndex), remainingVars),

			// Technical
			SupplyTemperature: tryGetTemperature(varDict, fmt.Sprintf("C%d_T%d_head1_supply_temp", controllerIndex, thermostatIndex), remainingVars),
			PWMOutput:         tryGetInt(varDict, fmt.Sprintf("C%d_T%d_ufh_pwm_output", controllerIndex, thermostatIndex), remainingVars),

			// Errors
			BatteryError:                tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_battery_error", controllerIndex, thermostatIndex), remainingVars),
			ValvePositionError:          tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_valve_position_err", controllerIndex, thermostatIndex), remainingVars),
			AirSensorError:              tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_air_sensor_error", controllerIndex, thermostatIndex), remainingVars),
			ExternalSensorError:         tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_external_sensor_err", controllerIndex, thermostatIndex), remainingVars),
			RelativeHumiditySensorError: tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_rh_sensor_error", controllerIndex, thermostatIndex), remainingVars),
			RfError:                     tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_rf_error", controllerIndex, thermostatIndex), remainingVars),
			TamperAlarm:                 tryGetBool(varDict, fmt.Sprintf("C%d_T%d_stat_tamper_alarm", controllerIndex, thermostatIndex), remainingVars),
		})

		// Debug
		// fmt.Println("TEST:", tryGetString(varDict, fmt.Sprintf("C%d_T%d_external_temperature", controllerIndex, thermostatIndex), remainingVars))
		// Remove variables
		delete(remainingVars, fmt.Sprintf("C%d_T%d_external_temperature", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_cool_allowed", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_cooling_allowed", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Monday", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Tuesday", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Wednesday", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Thursday", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Friday", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Saturday", controllerIndex, thermostatIndex))
		delete(remainingVars, fmt.Sprintf("C%d_T%d_Sunday", controllerIndex, thermostatIndex))
	}

	if printUnused {
		printVars(remainingVars)
	}

	return &data, nil
}

func printVars(vars map[string]string) {

	for key, value := range vars {
		fmt.Printf("\t%s: %s\n", key, value)
	}
}

func tryGetString(dictionary map[string]string, index string, remainingVars map[string]string) string {
	if value, ok := dictionary[index]; ok {
		delete(remainingVars, index)
		return value
	}
	return "n/a"
}

func tryGetTemperature(dictionary map[string]string, index string, remainingVars map[string]string) float32 {
	defaultValue := float32(math.MaxFloat32)

	if value, ok := dictionary[index]; ok {
		delete(remainingVars, index)
		val, err := strconv.ParseFloat(value, 32)
		if err != nil {
			return defaultValue
		}

		// conversion copied from here: https://github.com/dunv/homeassistant-uponor/blob/master/custom_components/uponor/__init__.py
		if val > 4508 {
			return defaultValue
		}
		return float32(math.Round((val-320)/18*100) / 100)
	}
	return defaultValue
}

func tryGetOffset(dictionary map[string]string, index string, remainingVars map[string]string) float32 {
	defaultValue := float32(math.MaxFloat32)

	if value, ok := dictionary[index]; ok {
		delete(remainingVars, index)
		val, err := strconv.ParseFloat(value, 32)
		if err != nil {
			return defaultValue
		}

		if val > 4508 {
			return defaultValue
		}
		return float32(math.Round((val)/18*100) / 100)
	}
	return defaultValue
}

func tryGetFloat(dictionary map[string]string, index string, remainingVars map[string]string) float32 {
	defaultValue := float32(math.MaxFloat32)

	if value, ok := dictionary[index]; ok {
		delete(remainingVars, index)
		val, err := strconv.ParseFloat(value, 32)
		if err != nil {
			return defaultValue
		}
		return float32(val)
	}
	return defaultValue
}

func tryGetInt(dictionary map[string]string, index string, remainingVars map[string]string) int32 {
	defaultValue := int32(math.MaxInt32)

	if value, ok := dictionary[index]; ok {
		delete(remainingVars, index)
		val, err := strconv.ParseInt(value, 10, 32)
		if err != nil {
			return defaultValue
		}
		return int32(val)
	}
	return defaultValue
}

func tryGetBool(dictionary map[string]string, index string, remainingVars map[string]string) bool {
	defaultValue := false

	if value, ok := dictionary[index]; ok {
		delete(remainingVars, index)
		val, err := strconv.ParseBool(value)
		if err != nil {
			return defaultValue
		}
		return val
	}
	return defaultValue
}
