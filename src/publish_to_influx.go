package main

import (
	"strconv"

	"github.com/dunv/ulog"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	api "github.com/influxdata/influxdb-client-go/v2/api"
)

func publishToInflux(writeAPI api.WriteAPI, data SMatrixData, cfg EnvConfig) {
	ulog.Tracef("Publishing ts %s", data.Time.String())

	p := influxdb2.NewPoint("uponor_system",
		map[string]string{},
		map[string]interface{}{
			"network_device":          data.NetworkDevice,
			"network_ip":              data.NetworkIP,
			"auto_balance":            data.AutoBalance,
			"enable_software_updates": data.EnableSoftwareUpdates,
			"firmware_version":        data.FirmwareVersion,
			"controllers":             len(data.Controllers),
			"rooms":                   len(data.Rooms),
		},
		data.Time,
	)
	writeAPI.WritePoint(p)

	for _, controller := range data.Controllers {
		p := influxdb2.NewPoint("uponor_controller",
			map[string]string{
				"name":            controller.Name,
				"controllerIndex": strconv.FormatInt(int64(controller.Index), 10),
				"present":         strconv.FormatBool(controller.Present),
			},
			map[string]interface{}{
				"general_system_alarm_error": controller.GeneralSystemAlarmError,
				"software_version":           controller.SoftwareVersion,
				"pending_software_version":   controller.PendingSoftwareVersion,
				"relays_config":              controller.RelaysConfig,
				"temperature_average":        controller.TemperateAverage,
			},
			data.Time,
		)
		writeAPI.WritePoint(p)
	}

	for _, room := range data.Rooms {
		p := influxdb2.NewPoint("uponor_room",
			map[string]string{
				"name":            room.Name,
				"controllerIndex": strconv.FormatInt(int64(room.ControllerIndex), 10),
				"thermostatIndex": strconv.FormatInt(int64(room.ThermostatIndex), 10),
				"present":         strconv.FormatBool(room.Present),
			},
			map[string]interface{}{
				"software_version":               room.SoftwareVersion,
				"temperature_current":            room.TemperatureCurrent,
				"temperature_desired":            room.TemperatureDesired,
				"relative_humidity":              room.RelativeHumidity,
				"utilization_factor":             room.UtilizationFactor,
				"actuator_active":                room.ActuatorActive,
				"pwm_output":                     room.PWMOutput,
				"battery_error":                  room.BatteryError,
				"valve_position_error":           room.ValvePositionError,
				"air_sensor_error":               room.AirSensorError,
				"external_sensor_error":          room.ExternalSensorError,
				"relative_humidity_sensor_error": room.RelativeHumiditySensorError,
				"rf_error":                       room.RfError,
				"tamper_alarm":                   room.TamperAlarm,
			},
			data.Time,
		)
		writeAPI.WritePoint(p)
	}

	writeAPI.Flush()
}
