#!/bin/bash

env $(grep -v '#' .env | xargs) go run -race ./*.go
