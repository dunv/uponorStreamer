package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"time"

	"github.com/codingconcepts/env"
	"github.com/dunv/ulog"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	api "github.com/influxdata/influxdb-client-go/v2/api"
)

func main() {
	cfg := EnvConfig{}
	if err := env.Set(&cfg); err != nil {
		ulog.Fatal(err)
	}

	location, err := time.LoadLocation(cfg.TimeZone)
	if err != nil {
		ulog.Fatalf("could not load timezone %s (%s)", cfg.TimeZone, err)
	}

	ulog.SetLogLevelFromString(cfg.LogLevel)
	ulog.LogEnvStruct(cfg, "\t")

	// Uponor-Client
	httpClient := &http.Client{
		Timeout: time.Second * time.Duration(60),
		Transport: &http.Transport{
			DialContext: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).DialContext,
			TLSHandshakeTimeout: 5 * time.Second,
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
			MaxIdleConns:        100,
			MaxIdleConnsPerHost: 100,
			IdleConnTimeout:     90 * time.Second,
		},
	}

	// Influx-Client
	var influxWriteAPI api.WriteAPI
	if cfg.InfluxEnable {
		influxClient := influxdb2.NewClientWithOptions(
			cfg.InfluxPublishAddress,
			fmt.Sprintf("%s:%s", cfg.InfluxUser, cfg.InfluxPassword),
			influxdb2.DefaultOptions().SetUseGZip(true).SetHTTPClient(httpClient),
		)
		influxWriteAPI = influxClient.WriteAPI("", cfg.InfluxDatabase)
	}

	go func() {
		for err := range influxWriteAPI.Errors() {
			ulog.Tracef("Err publishing to influxDB (%s)", err)
		}
	}()

	for {
		timing := time.Now()
		ulog.Tracef("Querying uponor...")
		apiResponse, err := getAttributes(httpClient, cfg.UponorServerIP)
		if err != nil {
			time.Sleep(cfg.UponorPollInterval)
			ulog.Errorf("Could not getAttributes (%s)", err)
			continue
		}
		ulog.Tracef("Querying uponor DONE (%s)", time.Since(timing))

		timing = time.Now()
		ulog.Tracef("Processing uponor...")
		data, err := processApiResponse(apiResponse, cfg.Debug, location)
		if err != nil {
			time.Sleep(cfg.UponorPollInterval)
			ulog.Errorf("Could not processApiRespone (%s)", err)
			continue
		}
		ulog.Tracef("Processing uponor DONE (%s)", time.Since(timing))

		if cfg.Debug {
			marshaled, err := json.MarshalIndent(data, "", "  ")
			if err != nil {
				time.Sleep(cfg.UponorPollInterval)
				ulog.Errorf("Could not marshal (%s)", err)
				continue
			}
			fmt.Println(string(marshaled))
		}

		if cfg.InfluxEnable {
			timing = time.Now()
			ulog.Tracef("Publishing ...")
			publishToInflux(influxWriteAPI, *data, cfg)
			ulog.Tracef("Publishing DONE (%s)", time.Since(timing))
		}
		time.Sleep(cfg.UponorPollInterval)
	}

}
