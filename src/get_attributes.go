package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

func getAttributes(client *http.Client, host string) (*SMatrixApiResponse, error) {
	r, err := http.NewRequest(http.MethodPost, fmt.Sprintf("http://%s/JNAP/", host), bytes.NewBufferString("{}"))
	if err != nil {
		return nil, err
	}
	r.Header.Set("Content-Type", "application/json")
	r.Header.Set("x-jnap-action", "http://phyn.com/jnap/uponorsky/GetAttributes")
	res, err := client.Do(r)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body := &SMatrixApiResponse{}
	err = json.NewDecoder(res.Body).Decode(body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
