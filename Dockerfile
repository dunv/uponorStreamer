FROM golang:1.17.0-alpine3.14 as builder
# build
WORKDIR /opt
ADD src ./
RUN go get -v
RUN GO111MODULE=on go build -ldflags="-s -w" -o /uponor_streamer

# add timezone-things
RUN apk add --update --no-cache upx tzdata zip ca-certificates
WORKDIR /usr/share/zoneinfo
RUN zip -r -0 /zoneinfo.zip .

# compress
RUN upx /uponor_streamer

# Build actual image
FROM alpine:3.14

# import env from build
ARG CI_COMMIT_SHA
ENV CI_COMMIT_SHA=$CI_COMMIT_SHA
ARG CI_BUILD_TIME
ENV CI_BUILD_TIME=$CI_BUILD_TIME
ARG VERSION
ENV VERSION=$VERSION

# add ca-certs and timezone-library
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /zoneinfo.zip /
ENV ZONEINFO /zoneinfo.zip

# add actual exe
WORKDIR /opt/
COPY --from=builder /uponor_streamer ./
ENTRYPOINT [ "/opt/uponor_streamer" ]
